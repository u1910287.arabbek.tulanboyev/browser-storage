import {cookies} from 'next/headers'
import styles from './style.module.scss'
export default function CookiesComp() {

    const hasAuth = cookies().has('auth')
    console.log('hasAuth', hasAuth)
    return (
        <div className={styles.container}>
            <h1>Cookies</h1>
            {!hasAuth ? (
                <div>
                    <p>Not logged in</p>
                    try to set cookie with a name auth from your browser
                    {/*<button>Login</button>*/}
                </div>
            ) : (
                <div>
                    <p>Logged in</p>
                    {/*<button>Logout</button>*/}
                </div>
            )}
        </div>
    )
}