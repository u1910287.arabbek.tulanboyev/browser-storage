'use client'

import styles from './styles.module.scss'
import {useEffect, useState} from "react";

export default function PlainCookies() {

    const [cookies, setCookies] = useState('')
    useEffect(() => {
        document.cookie = "name=Arabbek; SameSite=None; Secure";
        document.cookie = "favorite_food=palov; SameSite=None; Secure";
    }, []);

    function showCookies() {
        setCookies(document.cookie)
    }

    function clearOutputCookies() {
        setCookies('')
    }

    return (
        <div className={styles.container}>
            <h1>Plain Cookies</h1>
            <button className={styles.showCookies} onClick={() => showCookies()}>Show Cookies</button>
            <button className={styles.clearCookies} onClick={() => clearOutputCookies()}>Clear Cookies</button>
            <p>{cookies}</p>

        </div>
    )
}