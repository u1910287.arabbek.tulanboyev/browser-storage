'use client'

import styles from './styles.module.scss'
import {useState} from "react";

export default function PlainLocalStorageComp() {
    const [storageValue, setStorageValue] = useState('')
    const [value, setValue] = useState('')

    function setLocalStorageValue(e) {
        e.preventDefault()
        localStorage.setItem('storage', value)
    }

    function getLocalStorageValue() {
        setStorageValue(localStorage.getItem('storage'))
    }

    return (
        <div className={styles.container}>
            <h1>Plain Local Storage</h1>
            <form onSubmit={(e) => {
                setLocalStorageValue(e)
            }}>
                <input value={value} onChange={(e) => {
                    setValue(e.target.value)
                }} type="text" placeholder="Enter a value"/>
                <button type="submit">Set Local Storage</button>
            </form>
            <button className={styles.getLocalStorageBtn} onClick={()=>getLocalStorageValue()}>Get Local Storage</button>
            <button className={styles.clearLocalStorage}>Clear Local Storage</button>
            <p>Local Storage Value: </p>
            <p>{storageValue}</p>
        </div>
    )
}