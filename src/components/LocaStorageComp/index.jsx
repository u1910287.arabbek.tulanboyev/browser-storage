'use client'

import useLocalStorage from "@/hooks/useLocalStorage";
import {Button, Input} from "@chakra-ui/react";
import {useState} from "react";
import styles from './styles.module.scss'

export default function LocalStorageComp() {
    const [localStorageValue, setLocalStorageValue] = useLocalStorage('storage', '')
    const [inputValue, setInputValue] = useState()
    return (
        <div className={styles.container}>
            <h1>Local Storage</h1>
            <form>
                <Input
                    value={inputValue}
                    placeholder="Enter value"
                    onChange={(e) => setInputValue(e.target.value)}
                />
                <Button
                    type="submit"
                    onClick={(e) => {
                        e.preventDefault()
                        setLocalStorageValue(inputValue)
                    }}
                >
                    Save
                </Button>
                <Button
                    type="submit"
                    variant={'outline'}
                    color={'red'}
                    onClick={(e) => {
                        e.preventDefault()
                        setLocalStorageValue('')
                    }}
                >
                    Clear
                </Button>
            </form>
            <br/>
            <br/>
            <p>{localStorageValue}</p>
        </div>
    )
}