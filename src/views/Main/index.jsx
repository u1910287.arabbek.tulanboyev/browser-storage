import styles from './styles.module.scss'
import LocalStorageComp from "@/components/LocaStorageComp";
import CookiesComp from "@/components/CookiesComp";
import Link from "next/link";

export default async function Main() {
  return (
    <main className={styles.main}>
        <header>
            <Link href={'/'}>Using Next js</Link>
            <Link href={'/plain'}>Using plain js</Link>
        </header>
        <div className={styles.body}>
            <LocalStorageComp/>
            <CookiesComp/>
        </div>
        {/*<Tabs variant='enclosed'>*/}
        {/*    <TabList>*/}
        {/*        <Tab>Using Nextjs</Tab>*/}
        {/*        <Tab>Using plain js</Tab>*/}
        {/*    </TabList>*/}
        {/*    <TabPanels>*/}
        {/*        <TabPanel>*/}
        {/*            <LocalStorageComp />*/}
        {/*            <CookiesComp />*/}
        {/*        </TabPanel>*/}
        {/*        <TabPanel>*/}
        {/*        </TabPanel>*/}
        {/*    </TabPanels>*/}
        {/*</Tabs>*/}
    </main>
  )
}
