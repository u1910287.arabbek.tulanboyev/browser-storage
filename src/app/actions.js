'use server'

import {cookies} from 'next/headers'

export async function createCookie(name, data) {
    cookies().set(name, data)
}

