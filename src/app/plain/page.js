import styles from './page.module.scss'
import Link from "next/link";
import LocalStorageComp from "@/components/LocaStorageComp";
import CookiesComp from "@/components/CookiesComp";
import PlainLocalStorageComp from "@/components/PlainLocalStorage";
import PlainCookies from "@/components/PlainCookies";

async function getData() {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts')
  return response.json()
}

export default async function About() {
  const posts = await getData()
  return (
      <main className={styles.main}>
          <header>
              <Link href={'/'}>Using Next js</Link>
              <Link href={'/plain'}>Using plain js</Link>
          </header>
          <div className={styles.body}>
              <PlainLocalStorageComp/>
              <PlainCookies />
          </div>

      </main>

  )
}
